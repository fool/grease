package net.foolz.grease.testlib.models

import java.sql.ResultSet
import java.time.Instant
import java.time.temporal.ChronoField

import net.foolz.grease.{JdbcDatabase, JdbcDatabaseAsync, Queries, QueriesAsync}

import scala.concurrent.{ExecutionContext, Future}

case class People(id: PeopleId, dateCreated: Instant, name: String)
case class PeopleId(i: Long) extends Id

class PeopleQueries(val db: JdbcDatabase) extends Queries[People] {
  import PeopleQueries._
  def mapSelectStar(resultSet: ResultSet): Seq[People] = PeopleQueries.mapSelectStar(resultSet)

  def getById(id: PeopleId): Option[People] = {
    selectOne(SelectByIdSql, Seq(id))
  }
  def getOneByName(name: String): Option[People] = {
    selectOne(SelectByNameSql, Seq(name))
  }


  def create(name: String): People = {
    val dateCreated = Instant.now().`with`(ChronoField.MILLI_OF_SECOND, 0)
    val id = PeopleId(insert(InsertSql, Seq(dateCreated, name)))
    People(id, dateCreated, name)
  }


  def countByName(name: String): Long = {
    count(CountSql, Seq(name))
  }

  def rename(id: PeopleId, newName: String): Boolean = {
    update(RenameSql, Seq(newName, id)) == 1
  }
}



class PeopleQueriesAsync(val db: JdbcDatabaseAsync)(implicit val ctx: ExecutionContext)  extends QueriesAsync[People] {
  import PeopleQueries._
  def mapSelectStar(resultSet: ResultSet): Seq[People] = PeopleQueries.mapSelectStar(resultSet)

  def getById(id: Long): Future[Option[People]] = {
    selectOne(SelectByIdSql, Seq(id))
  }

  def getOneByName(name: String): Future[Option[People]] = {
    selectOne(SelectByNameSql, Seq(name))
  }


  def create(name: String): Future[People] = {
    val dateCreated = Instant.now().`with`(ChronoField.MILLI_OF_SECOND, 0)
    insert(InsertSql, Seq(dateCreated, name))
      .map(id => People(PeopleId(id), dateCreated, name))
  }

  def countByName(name: String): Future[Long] = {
    count(CountSql, Seq(name))
  }

  def rename(id: Long, newName: String): Future[Boolean] = {
    update(RenameSql, Seq(newName, id)).map(_ == 1)
  }
}

object PeopleQueries {
  val Id = "`Id`"
  val DateCreated = "`DateCreated`"
  val Name = "`Name`"
  val Star = s"$Id, $DateCreated, $Name"
  val Table = "`people`"
  val CreateTable: String = s"""
                      |CREATE TABLE $Table (
                      |  $Id integer auto_increment primary key,
                      |  $DateCreated datetime not null DEFAULT CURRENT_TIMESTAMP,
                      |  $Name varchar(255) not null
                      |);
                    """.stripMargin

  def mapSelectStar(resultSet: ResultSet): Seq[People] = {
    val people = Seq.newBuilder[People]
    while (resultSet.next) {
      people += People(
        PeopleId(resultSet.getLong(1)),
        resultSet.getTimestamp(2).toInstant,
        resultSet.getString(3)
      )
    }
    people.result()
  }

  val SelectByIdSql = s"SELECT $Star from $Table WHERE $Id = ?"
  val SelectByNameSql = s"SELECT $Star from $Table WHERE $Name = ?"
  val InsertSql = s"INSERT INTO $Table ($DateCreated, $Name) VALUES (?, ?)"
  val CountSql = s"SELECT COUNT(*) from $Table WHERE $Name = ?"
  val RenameSql = s"UPDATE $Table SET $Name = ? WHERE $Id = ?"
}