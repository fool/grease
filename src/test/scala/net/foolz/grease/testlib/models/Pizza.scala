package net.foolz.grease.testlib.models

import java.sql.ResultSet
import java.time.Instant
import java.time.temporal.ChronoField

import net.foolz.grease.testlib.models.PizzaToppings.PizzaTopping
import net.foolz.grease.testlib.{Dialect, H2, Mysql}
import net.foolz.grease.{Bitmask, BitmaskFlag, JdbcDatabase, Queries}

case class Pizza(id: PizzaId, created: Instant, toppings: PizzaToppings)
case class PizzaId(i: Long) extends Id

class PizzaQueries(val db: JdbcDatabase with Dialect) extends Queries[Pizza] {
  import PizzaQueries._
  def mapSelectStar(resultSet: ResultSet): Seq[Pizza] = PizzaQueries.mapSelectStar(resultSet)

  private val SelectByIdSql = s"""SELECT $Star FROM $Table WHERE $Id = ?"""
  def getById(id: PizzaId): Option[Pizza] = {
    selectOne(SelectByIdSql, Seq(id))
  }

  /**
    * H2 and Maria have differing syntax for bitwise AND
    */
  private def selectByToppingsSql(toppings: Seq[PizzaTopping], join: String): String = {
    if (toppings.isEmpty) {
      s"""SELECT $Star FROM $Table"""
    } else {
      db match {
        case _: Mysql =>
          s"""SELECT $Star FROM $Table WHERE """.concat(
            toppings.map(t => s"""$Toppings & ${t.power} = ?""").mkString(s" $join "))

        case _: H2 =>
          s"""SELECT $Star FROM $Table WHERE """.concat(
            toppings.map(t => s"""BITAND($Toppings, ${t.power}) = ?""").mkString(s" $join "))
      }
    }
  }

  /** Pizzas that have all of these toppings */
  def containsAllToppings(toppings: Seq[PizzaTopping]): Seq[Pizza] = {
    selectStar(selectByToppingsSql(toppings, "AND"), toppings)
  }

  /** Pizzas that have any of these toppings */
  def containsAnyToppings(toppings: Seq[PizzaTopping]): Seq[Pizza] = {
    selectStar(selectByToppingsSql(toppings, "OR"), toppings)
  }

  private val InsertSql = s"""INSERT INTO $Table ($DateCreated, $Toppings) VALUES (?, ?)"""
  def create(toppings: PizzaToppings): Pizza = {
    val dateCreated = Instant.now().`with`(ChronoField.MILLI_OF_SECOND, 0)
    val id = PizzaId(insert(InsertSql, Seq(dateCreated, toppings)))
    Pizza(id, dateCreated, toppings)
  }

  private val ChangeToppingsSql = s"""UPDATE $Table SET $Toppings = ? WHERE $Id = ?"""
  def changeToppings(id: Long, newToppings: PizzaToppings): Boolean = {
    update(ChangeToppingsSql, Seq(newToppings, id)) == 1
  }
}

object PizzaQueries {
  val Id = "`Id`"
  val DateCreated = "`DateCreated`"
  val Toppings = "`Toppings`"
  val Star = s"$Id, $DateCreated, $Toppings"
  val Table = "`pizza`"
  val CreateTable: String = s"""
                               |CREATE TABLE $Table (
                               |  $Id int(10) unsigned auto_increment primary key,
                               |  $DateCreated datetime not null DEFAULT CURRENT_TIMESTAMP,
                               |  $Toppings int(10) unsigned not null
                               |);
                    """.stripMargin

  def mapSelectStar(resultSet: ResultSet): Seq[Pizza] = {
    val people = Seq.newBuilder[Pizza]
    while (resultSet.next) {
      people += Pizza(
        PizzaId(resultSet.getLong(1)),
        resultSet.getTimestamp(2).toInstant,
        PizzaToppings(resultSet.getBigDecimal(3).toBigInteger)
      )
    }
    people.result()
  }
}



case class PizzaToppings(v: BigInt)
extends Bitmask[PizzaTopping, PizzaToppings](v, PizzaToppings.apply)

object PizzaToppings {
  sealed abstract class PizzaTopping(val index: Int) extends BitmaskFlag
  case object Cheese extends PizzaTopping(0)
  case object Pepperoni extends PizzaTopping(1)
  case object Chicken extends PizzaTopping(2)
  case object SunDriedTomato extends PizzaTopping(3)
  case object ArtichokeHeart extends PizzaTopping(4)
  case object RedOnion extends PizzaTopping(5)
  case object GreenOnion extends PizzaTopping(6)
  case object Garlic extends PizzaTopping(7)
  case object Pepperoncini extends PizzaTopping(8)
  case object Jalepeno extends PizzaTopping(9)

  def apply(toppings: Seq[PizzaTopping]): PizzaToppings = {
    PizzaToppings(toppings.foldLeft(BigInt(0))((v, topping) => v.setBit(topping.index)))
  }
}
