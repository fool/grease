package net.foolz.grease.testlib.models

import java.sql.ResultSet
import java.time.Instant

import net.foolz.grease.{JdbcDatabase, JdbcDatabaseAsync, Queries, QueriesAsync}

import scala.concurrent.{ExecutionContext, Future}

case class Car(id: CarId, dateCreated: Instant, model: String, owner: PeopleId)
case class CarId(i: Long) extends Id

class CarQueries(val db: JdbcDatabase) extends Queries[Car] {
  import CarQueries._
  def mapSelectStar(resultSet: ResultSet): Seq[Car] = CarQueries.mapSelectStar(resultSet)

  def getById(id: CarId): Option[Car] = {
    selectOne(SelectByIdSql, Seq(id))
  }
}
class CarQueriesAsync(val db: JdbcDatabaseAsync)(implicit val ctx: ExecutionContext) extends QueriesAsync[Car] {
  import CarQueries._
  def mapSelectStar(resultSet: ResultSet): Seq[Car] = CarQueries.mapSelectStar(resultSet)

  def getById(id: Long): Future[Option[Car]] = {
    selectOne(SelectByIdSql, Seq(id))
  }
}

object CarQueries {
  val Table = "`cars`"
  val Id = "`Id`"
  val DateCreated = "`DateCreated`"
  val Model = "`Model`"
  val Owner = "`Owner`"

  val Star = s"$Id, $DateCreated, $Model, $Owner"
  val CreateTable: String =
                    s"""
                       |CREATE TABLE $Table (
                       |  $Id integer auto_increment primary key,
                       |  $DateCreated datetime not null DEFAULT CURRENT_TIMESTAMP,
                       |  $Model varchar(255) not null,
                       |  $Owner integer not null
                       |);
                    """.stripMargin

  def mapSelectStar(resultSet: ResultSet): Seq[Car] = {
    val cars = Seq.newBuilder[Car]
    while (resultSet.next) {
      cars += Car(
        CarId(resultSet.getLong(1)),
        resultSet.getTimestamp(2).toInstant,
        resultSet.getString(3),
        PeopleId(resultSet.getLong(4))
      )
    }
    cars.result()
  }

  val SelectByIdSql = s"SELECT $Star from $Table WHERE $Id = ?"

}
