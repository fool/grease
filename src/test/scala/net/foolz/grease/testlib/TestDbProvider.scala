package net.foolz.grease.testlib

import net.foolz.grease.{JdbcDatabase, JdbcDatabaseAsync}

trait TestDbProvider {
  def withDbs[A](callback: (String, JdbcDatabase with Dialect) => A): Unit = {
    Map(
      "H2" -> TestH2Database.createDb(),
      "Maria" -> TestMariaDatabase.createDb()
    ).foreach(db => callback(db._1, db._2))
  }
}

trait TestDbProviderAsync {
  def withAsyncDbs[A](callback: (String, JdbcDatabaseAsync with Dialect) => A): Unit = {
    Map(
      "H2" -> TestH2Database.createDbAsync(),
      "Maria" -> TestMariaDatabase.createDbAsync()
    ).foreach(db => callback(db._1, db._2))
  }

}