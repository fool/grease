package net.foolz.grease.testlib


import java.sql.{DriverManager, PreparedStatement}
import java.util.UUID
import java.util.concurrent.{Executors, TimeUnit}

import ch.vorburger.mariadb4j.{DB, DBConfigurationBuilder}
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import javax.sql.DataSource
import net.foolz.grease.testlib.models.Id
import net.foolz.grease.{JdbcDatabase, JdbcDatabaseAsync}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration


class TestMariaDatabase(val dataSource: DataSource) extends JdbcDatabase with Mysql {
  override def prepareAny(index: Int, any: Any, stmt: PreparedStatement): Unit = {
    any match {
      case i: Id => stmt.setLong(index, i.i)
      case _ => super.prepareAny(index, any, stmt)
    }
  }
}

class TestMariaDatabaseAsync(val dataSource: DataSource) extends JdbcDatabaseAsync with Mysql {
  implicit def dbCtx: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(1))
  override def prepareAny(index: Int, any: Any, stmt: PreparedStatement): Unit = {
    any match {
      case i: Id => stmt.setLong(index, i.i)
      case _ => super.prepareAny(index, any, stmt)
    }
  }
}

object TestMariaDatabase {
  def createDb(): TestMariaDatabase = new TestMariaDatabase(inMemoryDatasource())
  def createDbAsync(): TestMariaDatabaseAsync = new TestMariaDatabaseAsync(inMemoryDatasource())

  private lazy val mariaConfig = synchronized {
    println(s"Starting up mariadb")
    val now = System.nanoTime()
    val config = DBConfigurationBuilder.newBuilder
    config.setPort(0)
    val db = DB.newEmbeddedDB(config.build())
    db.start()
    val duration = new FiniteDuration(System.nanoTime() - now, TimeUnit.NANOSECONDS)
    println(s"mariadb started in ${duration.toMillis}ms")
    config
  }

  /** Only one server is started but there's a new DB instance for every test */
  def inMemoryDatasource(autoCommit: Boolean = false, minPoolSize: Int = 1, maxPoolSize: Int = 1): DataSource = {
    val DbName = s"maria_unit_test_${UUID.randomUUID().toString.replaceAllLiterally("-","")}"
    val conn = DriverManager.getConnection(mariaConfig.getURL("test"), "root", "")
    val stmt = conn.prepareStatement(s"CREATE DATABASE `$DbName`")
    stmt.execute()

    val config = new HikariConfig
    config.setJdbcUrl(mariaConfig.getURL(DbName))
    config.setUsername("root")
    config.setPassword("")
    config.setAutoCommit(autoCommit)
    config.setMinimumIdle(minPoolSize)
    config.setMaximumPoolSize(maxPoolSize)
    config.setPoolName(s"test-maria")
    new HikariDataSource(config)
  }
}

