package net.foolz.grease.testlib

/** A helper to write DB-specific queries where syntax may change */
sealed trait Dialect
trait Mysql extends Dialect
trait H2 extends Dialect
