package net.foolz.grease.testlib

import java.sql.PreparedStatement
import java.util.UUID
import java.util.concurrent.Executors

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import javax.sql.DataSource
import net.foolz.grease.testlib.models.Id
import net.foolz.grease.{JdbcDatabase, JdbcDatabaseAsync}

import scala.concurrent.ExecutionContext


class   TestH2Database(val dataSource: DataSource)
extends JdbcDatabase
with    H2 {
  override def prepareAny(index: Int, any: Any, stmt: PreparedStatement): Unit = {
    any match {
      case i: Id => stmt.setLong(index, i.i)
      case _ => super.prepareAny(index, any, stmt)
    }
  }
}

class TestH2DatabaseAsync(val dataSource: DataSource) extends JdbcDatabaseAsync with H2 {
  implicit def dbCtx: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(1))
  override def prepareAny(index: Int, any: Any, stmt: PreparedStatement): Unit = {
    any match {
      case i: Id => stmt.setLong(index, i.i)
      case _ => super.prepareAny(index, any, stmt)
    }
  }
}

object TestH2Database {

  Class.forName("org.h2.Driver")
  def createDb(): TestH2Database = new TestH2Database(inMemoryDatasource())
  def createDbAsync(): TestH2DatabaseAsync = new TestH2DatabaseAsync(inMemoryDatasource())

  def inMemoryDatasource(autoCommit: Boolean = false, minPoolSize: Int = 1, maxPoolSize: Int = 3): DataSource = {
    val config = new HikariConfig
    val dbName = UUID.randomUUID().toString.replaceAllLiterally("-","")
    config.setJdbcUrl(s"jdbc:h2:mem:db$dbName?MODE=MySQL")
    config.setUsername("username")
    config.setPassword("password")
    config.setAutoCommit(autoCommit)
    config.setMinimumIdle(minPoolSize)
    config.setMaximumPoolSize(maxPoolSize)
    config.setPoolName(s"test")

    new HikariDataSource(config)
  }
}
