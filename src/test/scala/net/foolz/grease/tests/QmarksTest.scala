package net.foolz.grease.tests

import net.foolz.grease.Qmarks
import org.scalatest.ParallelTestExecution
import org.scalatest.funsuite.AnyFunSuite

class QmarksTest extends AnyFunSuite with ParallelTestExecution {
  test("Qmarks(0) should give ()") {
    assertResult("()")(Qmarks(0))
  }
  test("Qmarks(1) should give (?)") {
    assertResult("(?)")(Qmarks(1))
  }
  test("Qmarks(2) should give (?,?)") {
    assertResult("(?,?)")(Qmarks(2))
  }
  test("Qmarks(3) should give (?,?,?)") {
    assertResult("(?,?,?)")(Qmarks(3))
  }


  test("Qmarks.groupList(0, 1) should give \"\"") {
    assertResult("")(Qmarks.groupList(0, 1))
  }
  test("Qmarks.groupList(1, 0) should give ()") {
    assertResult("()")(Qmarks.groupList(1, 0))
  }
  test("Qmarks.groupList(1, 1) should give (?)") {
    assertResult("(?)")(Qmarks.groupList(1, 1))
  }
  test("Qmarks.groupList(1, 2) should give (?,?)") {
    assertResult("(?,?)")(Qmarks.groupList(1, 2))
  }
  test("Qmarks.groupList(2, 1) should give (?),(?)") {
    assertResult("(?),(?)")(Qmarks.groupList(2, 1))
  }
  test("Qmarks.groupList(2, 2) should give (?,?),(?,?)") {
    assertResult("(?,?),(?,?)")(Qmarks.groupList(2, 2))
  }
}
