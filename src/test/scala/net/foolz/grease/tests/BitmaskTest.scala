package net.foolz.grease.tests

import net.foolz.grease.testlib.TestDbProvider
import net.foolz.grease.testlib.models.{PizzaQueries, PizzaToppings}
import org.scalatest.funsuite.AnyFunSuite

class BitmaskTest extends AnyFunSuite with TestDbProvider {
  import PizzaQueries._
  import net.foolz.grease.testlib.models.PizzaToppings._
  withDbs {
    (name, db) =>
      test(s"$name Bitmask flags should create from values") {
        db.execute(CreateTable)
        val pizzas = new PizzaQueries(db)
        val toppings = PizzaToppings(Seq(Cheese, Chicken, Jalepeno))
        pizzas.create(toppings)
        // Cheese   = index 0  value 1
        // Chicken  = index 2  value 4
        // Jalepeno = index 9  value 512
        val expected = 1 | 4 | 512
        val pizza = db.select(s"""SELECT $Star FROM $Table WHERE $Toppings = ?""", Seq(expected))(PizzaQueries.mapSelectStar).head
        assertResult(toppings)(pizza.toppings)
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name Bitmask flags should select and filter properly") {
        db.execute(CreateTable)
        val pizzas = new PizzaQueries(db)
        val p1 = pizzas.create(PizzaToppings(Seq(Cheese, Chicken, Jalepeno, GreenOnion)))
        val p2 = pizzas.create(PizzaToppings(Seq(Cheese, SunDriedTomato, ArtichokeHeart, Pepperoncini)))
        val p3 = pizzas.create(PizzaToppings(Seq(Cheese, SunDriedTomato, Jalepeno)))
        val p4 = pizzas.create(PizzaToppings(Seq(Cheese, Garlic, RedOnion)))
        val p5 = pizzas.create(PizzaToppings(Seq(Cheese, GreenOnion, Jalepeno, ArtichokeHeart)))

        val cheesePizzas = pizzas.containsAllToppings(Seq(Cheese))
        val expectedCheesePizzas = Seq(p1, p2, p3, p4, p5)
        assertResult(expectedCheesePizzas)(cheesePizzas)

        val sunDriedTomatoPizzas = pizzas.containsAllToppings(Seq(SunDriedTomato))
        val expectedSunDriedTomatoPizzas = Seq(p2, p3)
        assertResult(expectedSunDriedTomatoPizzas)(sunDriedTomatoPizzas)

        val greenPizzas = pizzas.containsAllToppings(Seq(GreenOnion, Jalepeno))
        val expectedGreenPizzas = Seq(p1, p5)
        assertResult(expectedGreenPizzas)(greenPizzas)

        val anyToppings = pizzas.containsAnyToppings(Seq(Garlic, Pepperoncini))
        assertResult(Seq(p2, p4))(anyToppings)
      }
  }
}
