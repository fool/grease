package net.foolz.grease.tests

import net.foolz.grease.testlib.TestDbProvider
import net.foolz.grease.testlib.models.PeopleQueries
import org.scalatest.funsuite.AnyFunSuite

class QueriesTest extends AnyFunSuite with TestDbProvider {
  withDbs {
    (name, db) =>
      test(s"$name People Query insert should return auto id") {
        val people = new PeopleQueries(db)
        db.execute(PeopleQueries.CreateTable)
        val name = "Jermiah Bullfrog"
        val jeremiah = people.create(name)
        val jeremiah2 = people.getOneByName(name).get
        assertResult(jeremiah)(jeremiah2)
      }
  }

  withDbs {
    (name, db) =>
    test(s"$name People Query count should return number of rows") {
      val people = new PeopleQueries(db)
      db.execute(PeopleQueries.CreateTable)
      val name = "Jermiah Bullfrog"
      0.until(20).foreach(_ => people.create(name))
      val total = people.countByName(name)
      assertResult(20)(total)
    }
  }

  withDbs {
    (name, db) =>
      test(s"$name People Query update should return rows affected when rows affected") {
        val people = new PeopleQueries(db)
        db.execute(PeopleQueries.CreateTable)
        val name = "Jermiah Bullfrog"
        val frog = people.create(name)
        people.rename(frog.id, "yoro")
        val newFrog = people.getById(frog.id).get
        assertResult("yoro")(newFrog.name)
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name People Query update should return rows affected when no rows affected") {
        val people = new PeopleQueries(db)
        db.execute(PeopleQueries.CreateTable)
        val name = "Jermiah Bullfrog"
        val frog = people.create(name)
        val badId = frog.id.copy(i = frog.id.i + 100)
        val result = people.rename(badId, "yoro")
        assertResult(false)(result)
      }
  }
}

