package net.foolz.grease.tests

import java.sql.SQLException
import net.foolz.grease.testlib.TestDbProvider
import net.foolz.grease.testlib.models.{CarId, CarQueries, PeopleId, PeopleQueries}
import org.scalatest.funsuite.AnyFunSuite

class JdbcDatabaseTest extends AnyFunSuite with TestDbProvider {
  import PeopleQueries._
  withDbs {
    (name, db) =>
    test(s"$name JDBC Database select should give all results") {
      db.execute(CreateTable)

      db.execute(s"INSERT INTO $Table ($Name) VALUES ('Homer Simpson')")
      db.execute(s"INSERT INTO $Table ($Name) VALUES ('Bart Simpson')")
      db.execute(s"INSERT INTO $Table ($Name) VALUES ('Homer Simpson')")

      val results = db.select(s"SELECT $Star from $Table where $Name = 'Homer Simpson'")(mapSelectStar)
      val expected = Seq((PeopleId(1), "Homer Simpson"), (PeopleId(3), "Homer Simpson"))
      assertResult(expected)(results.map(p => (p.id, p.name)))

      val results2 = db.select(s"SELECT $Star from $Table where $Name = 'Bart Simpson'")(mapSelectStar)
      val expected2 = Seq((PeopleId(2), "Bart Simpson"))
      assertResult(expected2)(results2.map(p => (p.id, p.name)))
    }
  }

  withDbs {
    (name, db) =>
    test(s"$name JDBC Database insertOneWithAutoId should Give back auto id after insert") {
      db.execute(CreateTable)

      val autoIdOption = db.insertOneWithAutoId(s"INSERT INTO $Table ($Name) VALUES ('Homer Simpson')")
      assert(autoIdOption.isDefined, s"The insert gave back None for auto ids, but there should have been one")
      val autoId = autoIdOption.get
      assertResult(1)(autoId)
    }
  }

  withDbs {
    (name, db) =>
      test(s"$name JDBC Database insertWithAutoId should Give back all the auto ids after a bulk insert") {
        db.execute(CreateTable)
        val autoIds = db.insertWithAutoId(
          s"""
             |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
             |""".stripMargin)
        val expected = Seq(1, 2, 3, 4, 5)
        assertResult(expected)(autoIds)
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name JDBC Database update should Give back rows affected") {
        db.execute(CreateTable)
        db.insertWithAutoId(
          s"""
             |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
             |""".stripMargin)
        val deletedRows = db.update(
          s"""
             |DELETE FROM $Table WHERE $Name LIKE '%Simpson%'
      """.stripMargin)
        val expected = 5
        assertResult(expected)(deletedRows)
      }
  }


  withDbs {
    (name, db) =>
      test(s"$name JDBC Database updateOne should Return true when one row is affected") {
        db.execute(CreateTable)
        db.insertWithAutoId(
          s"""
             |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
             |""".stripMargin)
        val result1 = db.updateOne(
          s"""
             |UPDATE $Table SET $Name = ? WHERE $Id = ?
      """.stripMargin, Seq("Milhouse Van Houten", 4))
        assert(result1, "Update failed")
        val selectResult = db.select(s"SELECT ${PeopleQueries.Star} from `people` WHERE ${PeopleQueries.Id} = 4")(PeopleQueries.mapSelectStar)
        assertResult("Milhouse Van Houten")(selectResult.head.name)
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name JDBC Database transaction nesting withStatement should transactionally update rows") {
        db.execute(CreateTable)
        db.execute(CarQueries.CreateTable)
        val (jeffId, carId) = db.withConnection {
          connection =>
            connection.setAutoCommit(false)
            val peopleSql = s"""INSERT INTO $Table ($Name) VALUES (?)"""
            val peopleValues = Seq("Jeff Gordon")
            db.withPreparedStatementGeneratingKeys(connection, peopleSql, peopleValues) {
              peopleStatement =>
                val jeffsId = PeopleId(db.insertWithAutoId(connection, peopleStatement, false).head)
                val carSql = s"""INSERT INTO ${CarQueries.Table} (${CarQueries.Model}, ${CarQueries.Owner}) VALUES(?, ?)"""
                val carValues = Seq("GTI", jeffsId)
                val carId = CarId(db.withPreparedStatementGeneratingKeys(connection, carSql, carValues) {
                  carStatement =>
                    db.insertWithAutoId(connection, carStatement, true).head
                })
                (jeffsId, carId)
            }
        }

        val p = new PeopleQueries(db)
        val c = new CarQueries(db)
        val jeff = p.getById(jeffId).get
        val car = c.getById(carId).get
        assertResult("Jeff Gordon")(jeff.name)
        assertResult(jeff.id)(car.owner)
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name JDBC Database prepare bad query should throw exception") {
        intercept[SQLException] {
          db.update("UPDATE FROM SELECT ORDER BY FROM")
        }
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name JDBC Database insert should return number of rows inserted") {
        db.execute(CreateTable)
        val rowsAffected = db.insert(
          s"""
             |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
             |""".stripMargin)

        assertResult(5, s"Inserting 5 simpsons should give 5 back")(rowsAffected)
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name JDBC Database insertOne should return True when one row inserted") {
        db.execute(CreateTable)
        val success = db.insertOne(
          s"""
             |INSERT INTO $Table ($Name) VALUES ('Homer Simpson')
             |""".stripMargin)

        assertResult(true)(success)
      }
  }

  withDbs {
    (name, db) =>
      test(s"$name JDBC Database insertOne should return False when more than one row inserted") {
        db.execute(CreateTable)
        val success = db.insertOne(
          s"""
             |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
             |""".stripMargin)

        assertResult(false)(success)
      }
  }
}
