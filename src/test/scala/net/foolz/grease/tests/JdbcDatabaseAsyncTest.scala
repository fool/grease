package net.foolz.grease.tests

import java.sql.SQLException
import net.foolz.grease.testlib.TestDbProviderAsync
import net.foolz.grease.testlib.models.{CarQueries, CarQueriesAsync, PeopleId, PeopleQueries, PeopleQueriesAsync}
import org.scalatest.Assertion
import org.scalatest.funsuite.AsyncFunSuite

import scala.concurrent.Future

class   JdbcDatabaseAsyncTest
extends AsyncFunSuite
with    TestDbProviderAsync {
  import net.foolz.grease.testlib.models.PeopleQueries._
  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database select should give all results" ) {
        for {
          _ <- db.execute(CreateTable)
          _ <- db.execute(s"INSERT INTO $Table ($Name) VALUES ('Homer Simpson')")
          _ <- db.execute(s"INSERT INTO $Table ($Name) VALUES ('Bart Simpson')")
          _ <- db.execute(s"INSERT INTO $Table ($Name) VALUES ('Homer Simpson')")

          results <- db.select(s"SELECT $Star from $Table where $Name = 'Homer Simpson'")(mapSelectStar)
          expected = Seq((PeopleId(1), "Homer Simpson"), (PeopleId(3), "Homer Simpson"))
          _ = assertResult(expected)(results.map(p => (p.id, p.name)))
          results2 <- db.select(s"SELECT $Star from $Table where $Name = 'Bart Simpson'")(mapSelectStar)
          expected2 = Seq((PeopleId(2), "Bart Simpson"))
        } yield assertResult(expected2)(results2.map(p => (p.id, p.name)))
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database insertOneWithAutoId should Give back auto id after insert") {
        for {
          _ <- db.execute(CreateTable)
          autoIdOption <- db.insertOneWithAutoId(s"INSERT INTO $Table ($Name) VALUES ('Homer Simpson')")
          _ = assert(autoIdOption.isDefined, s"The insert gave back None for auto ids, but there should have been one")
          autoId = autoIdOption.get
        } yield assertResult(1)(autoId)
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database insertWithAutoId should Give back all the auto ids after a bulk insert") {
        for {
          _ <- db.execute(CreateTable)
          autoIds <- db.insertWithAutoId(
            s"""
               |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
               |""".stripMargin)
          expected = Seq(1, 2, 3, 4, 5)
        } yield assertResult(expected)(autoIds)
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database update should Give back rows affected") {
        for {


          _ <- db.execute(CreateTable)
          _ <- db.insertWithAutoId(
            s"""
               |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
               |""".stripMargin)
          deletedRows <- db.update(
            s"""
               |DELETE FROM $Table WHERE $Name LIKE '%Simpson%'
      """.stripMargin)
        } yield assertResult(5)(deletedRows)
      }
  }


  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database updateOne should Return true when one row is affected") {
        for {
          _ <- db.execute(CreateTable)
          _ <- db.insertWithAutoId(
            s"""
               |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
               |""".stripMargin)
          result1 <- db.updateOne(
            s"""
               |UPDATE $Table SET $Name = ? WHERE $Id = ?
        """.stripMargin, Seq("Milhouse Van Houten", 4))
          _ = assert(result1, "Update failed")
          selectResult <- db.select(s"SELECT ${PeopleQueries.Star} from `people` WHERE ${PeopleQueries.Id} = 4")(PeopleQueries.mapSelectStar)
        } yield assertResult("Milhouse Van Houten")(selectResult.head.name)
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database transaction nesting withStatement should transactionally update rows") {
        val p = new PeopleQueriesAsync(db)
        val c = new CarQueriesAsync(db)

        for {
          _ <- db.execute(CreateTable)
          _ <- db.execute(CarQueries.CreateTable)
          (jeffId, carId) <- db.withConnection {
            connection =>
              connection.setAutoCommit(false)
              val peopleSql = s"""INSERT INTO $Table ($Name) VALUES (?)"""
              val peopleValues = Seq("Jeff Gordon")
              db.withPreparedStatementGeneratingKeys(connection, peopleSql, peopleValues) {
                peopleStatement =>
                  for {
                    jeffsIds <- db.insertWithAutoId(connection, peopleStatement, false)
                    jeffsId = jeffsIds.head
                    carSql = s"""INSERT INTO ${CarQueries.Table} (${CarQueries.Model}, ${CarQueries.Owner}) VALUES(?, ?)"""
                    carValues = Seq("GTI", jeffsId)
                    carIds <- db.withPreparedStatementGeneratingKeys(connection, carSql, carValues) {
                      carStatement =>
                        db.insertWithAutoId(connection, carStatement, true)
                    }
                    carId = carIds.head
                  } yield (jeffsId, carId)
              }
          }


          jeffO <- p.getById(jeffId)
          jeff = jeffO.get
          carO <- c.getById(carId)
          car = carO.get
          _ = assertResult("Jeff Gordon")(jeff.name)
        } yield assertResult(jeff.id)(car.owner)
      }
  }


  private def assertFutureFailedWithMyException(future: Future[_]): Future[Assertion] = {
    future.map(_ => fail(s"Should not successfully run this query"))
      .recover({
        case t => assert(t.isInstanceOf[MySpecificException])
      })
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database prepare bad query should not throw and give a failed Future") {
        val f = db.insert("INSERT SELECT DELETE DROP TABLE FROM")
        f.map(_ => fail(s"Should not successfully run this query"))
          .recover({ case t: SQLException => assert(true) })
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database withConnection should never throw exceptions and give a failed Future") {
        assertFutureFailedWithMyException(db.withConnection {
          _ => throw MySpecificException("boom")
        })
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database withStatement should never throw exceptions and give a failed Future") {
        db.withConnection {
          connection =>
            val statement = connection.prepareStatement("SELECT 1")
            assertFutureFailedWithMyException(db.withStatement(statement) {
              _ => throw MySpecificException("boom")
            })
        }
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database withPreparedStatement should never throw exceptions and give a failed Future") {
        db.withConnection {
          connection =>
            assertFutureFailedWithMyException(db.withPreparedStatement(connection, "SELECT 1", Seq.empty) {
              _ => throw MySpecificException("boom")
            })
        }
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database withPreparedStatementGeneratingKeys should never throw exceptions and give a failed Future") {
        db.withConnection {
          connection =>
            assertFutureFailedWithMyException(db.withPreparedStatementGeneratingKeys(connection, "SELECT 1", Seq.empty) {
              _ => throw MySpecificException("boom")
            })
        }
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database insert should return number of rows inserted") {
        for {
          _ <- db.execute(CreateTable)
          rowsAffected <- db.insert(
            s"""
               |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
               |""".stripMargin)
        } yield assertResult(5, s"Inserting 5 simpsons should give 5 back")(rowsAffected)
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database insertOne should return True when one row inserted") {
        for {
          _ <- db.execute(CreateTable)
          success <- db.insertOne(
            s"""
               |INSERT INTO $Table ($Name) VALUES ('Homer Simpson')
               |""".stripMargin)
        } yield assertResult(true)(success)
      }
  }

  withAsyncDbs {
    (name, db) =>
      test(s"$name Async JDBC Database insertOne should return False when more than one row inserted") {
        for {
          _ <- db.execute(CreateTable)
          success <- db.insertOne(
            s"""
               |INSERT INTO $Table ($Name) VALUES ('Homer Simpson'), ('Marge Simpson'), ('Bart Simpson'), ('Lisa Simpson'), ('Maggie Simpson')
               |""".stripMargin)
        } yield assertResult(false)(success)
      }
  }
}

case class MySpecificException(message: String) extends Exception(message)
