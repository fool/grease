package net.foolz.grease



import java.io.InputStream
import java.sql.{PreparedStatement, Timestamp}
import java.time.Instant

/**
  * Thrown when a value in your prepared statement could not be automatically prepared
  * @param message What went wrong
  * @param t       Underlying exception that was thrown by JDBC library
  */
case class PreparedStatementValueException(message: String, t: Throwable) extends Exception(message, t)

/** Automatically prepares statements */
trait PreparedStatementGenerator {
  /**
    * Automatically binds all query parameters to a prepared statement
    * @param statement Statement to bind with
    * @param sql       The query, not used, only for debugging
    * @param data      The data to bind
    */
  def autoPrepare(statement: PreparedStatement, sql: String, data: Seq[Any]): Unit = {
    data.zipWithIndex.foreach {
      case (value, index0) =>
        try {
          prepareAny(index0 + 1, value, statement)
        } catch {
          /* re-throw with query so you can find out what caused this easily */
          case t: Throwable => throw PreparedStatementValueException(s"Exception while preparing query at variable $index0 with value ${value.getClass.getName} $value in query $sql: ${t.getClass.getName} ${t.getMessage}", t)
        }
    }
  }

  /**
    * Binds a query parameter to a prepared statement.
    * Override this method in your subclass to add custom types to be prepared automatically
    * call super if you want these base types supported
    *
    * This can throw many different exceptions
    *
    * @param index What parameter number this is, starts at 1
    * @param any   The parameter value to encode
    * @param stmt  The prepared statement
    */
  def prepareAny(index: Int, any: Any, stmt: PreparedStatement): Unit = {
    any match {
      case s: String => stmt.setString(index, s)
      case i: Int => stmt.setInt(index, i)
      case i: Long => stmt.setLong(index, i)
      case f: Float => stmt.setFloat(index, f)
      case d: Double => stmt.setDouble(index, d)
      case b: Boolean => stmt.setBoolean(index, b)
      case i: BigInt => stmt.setLong(index, i.longValue) /* only safe if your BigInt fits in a long */
      case d: BigDecimal => stmt.setBigDecimal(index, d.bigDecimal)
      case b: Bitmask[_, _] => stmt.setLong(index, b.value.longValue) /* only safe if your BigInt fits in a long */
      case b: BitmaskFlag => stmt.setLong(index, b.power.longValue) /* only safe if your BigInt fits in a long */
      case i: InputStream => stmt.setBinaryStream(index, i)
      case i: Instant => stmt.setTimestamp(index, Timestamp.from(i))
      case Some(v) => prepareAny(index, v, stmt)
      case None => stmt.setString(index, null)
      case other: Any => throw new Exception(s"Unsupported datatype ${other.getClass.getName} at index ${index-1}")
      case null => stmt.setString(index, null)
    }
  }
}
