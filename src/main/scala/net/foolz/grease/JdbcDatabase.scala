package net.foolz.grease


import java.sql.{Connection, PreparedStatement, ResultSet, Statement}

import javax.sql.DataSource

import scala.collection.mutable.ListBuffer


/**
  * Small wrapper over JDBC with a few helpers for the most common query types
  */
trait JdbcDatabase extends PreparedStatementGenerator {
  /** Any JDBC implementation will be able to give you a DataSource object */
  def dataSource: DataSource

  /**
    * Create a Connection that is always released
    * @param function Callback to use the function in
    * @return         The result of your callback function
    */
  def withConnection[R](function: Connection => R): R = {
    val connection = dataSource.getConnection
    try {
      function(connection)
    } finally {
      if (connection != null && !connection.isClosed) {
        connection.close()
      }
    }
  }

  /**
    * Use a PreparedStatement that is always closed when your callback is done
    * @param statement The statement
    * @param function  Callback function
    * @return          The result of your callback function
    */
  def withStatement[R](statement: PreparedStatement)(function: PreparedStatement => R): R = {
    try {
      function(statement)
    } finally {
      if (statement != null && !statement.isClosed) {
        statement.close()
      }
    }
  }

  /**
    * Prepare a query using autoPrepare() that will always close the prepared statement. This version may not
    * return auto generated keys from the ResultSet. Behavior depends on your JDBC Driver
    * @param connection Your active SQL connection
    * @param sql        Your query
    * @param data       Your query's data
    * @param function   Callback to use the prepared statement in
    * @return           Result of your callback function
    */
  def withPreparedStatement[R](connection: Connection, sql: String, data: Seq[Any])(function: PreparedStatement => R): R = {
    withStatement(connection.prepareStatement(sql)) {
      statement =>
        autoPrepare(statement, sql, data)
        function(statement)
    }
  }

  /**
    * Prepare a query that should return auto generated ids (ex: Auto Increment Primary Key) using autoPrepare() and
    * always close the prepared statement.
    * @param connection Your active SQL connection
    * @param sql        Your query
    * @param data       Your query's data
    * @param function   Callback to use the prepared statement in
    * @return           Result of your callback function
    */
  def withPreparedStatementGeneratingKeys[R](connection: Connection, sql: String, data: Seq[Any])(function: PreparedStatement => R): R = {
    withStatement(connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
      statement =>
        autoPrepare(statement, sql, data)
        function(statement)
    }
  }

  /**
    * SELECT Query where your callback function is not asynchronous
    * @param sql Your query
    * @param data Your query's data
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done. This must be a synchronous function
    * @return The result of your callback function
    */
  def select[R](sql: String, data: Seq[Any] = Seq.empty)(callback: ResultSet => R): R = {
    withConnection {
      connection =>
        withPreparedStatement(connection, sql, data) {
          statement =>
            select(connection, statement)(callback)
        }
    }
  }

  /**
    * SELECT Query accepting a Connection is useful for selecting within an active transaction
    * @param connection Active db connection
    * @param statement  Prepared query
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done. This must be a synchronous function
    * @return The result of your callback function
    */
  def select[R](connection: Connection, statement: PreparedStatement)(callback: ResultSet => R): R = {
    val resultSet = statement.executeQuery()
    if (!connection.getAutoCommit) {
      connection.commit()
    }
    callback(resultSet)
  }

  /**
    * SELECT Query accepting a Connection is useful for selecting within an active transaction
    * @param connection Active db connection
    * @param query Your query
    * @param data Your query's data
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done. This must be a synchronous function
    * @return The result of your callback function
    */
  def select[R](connection: Connection, query: String, data: Seq[Any])(callback: ResultSet => R): R = {
    withPreparedStatement(connection, query, data) {
      statement =>
        select(connection, statement)(callback)
    }
  }

  /**
    * Execute an update query. Update means anything that's not a SELECT
    * @param sql  Your query
    * @param data Your query's data
    * @return Number of rows affected
    */
  def update(sql: String, data: Seq[Any] = Seq.empty): Int = {
    withConnection {
      connection =>
        withPreparedStatement(connection, sql, data) {
          statement =>
            update(connection, statement, true)
        }
    }
  }

  /**
    * Execute an update query with an active connection. Update means anything that's not a SELECT.
    * This is useful for transactions
    * @param connection Active db connection
    * @param statement  Prepared query
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return Number of rows affected
    */
  def update(connection: Connection, statement: PreparedStatement, commit: Boolean): Int = {
    val rowsAffected = statement.executeUpdate()
    if (!connection.getAutoCommit && commit) {
      connection.commit()
    }
    rowsAffected
  }

  /**
    * Execute an update query with an active connection. Update means anything that's not a SELECT.
    * This is useful for transactions
    * @param connection Active db connection
    * @param query  Your query
    * @param data Your query's data
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return Number of rows affected
    */
  def update(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Int = {
    withPreparedStatement(connection, query, data) {
      statement =>
        update(connection, statement, commit)
    }
  }

  /**
    * Execute an update query (Update means anything that's not a SELECT) and test if the
    * rows affected was 1
    * @param sql  Your query
    * @param data Your query's data
    * @return True if one row was affected by this query, false otherwise
    */
  def updateOne(sql: String, data: Seq[Any] = Seq.empty): Boolean = {
    update(sql, data) == 1
  }

  /**
    * Execute an update query (Update means anything that's not a SELECT) and test if the
    * rows affected was 1 using an active connection. This is useful for transactions
    * @param connection Active db connection
    * @param statement  Prepared query
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return True if one row was affected by this query, false otherwise
    */
  def updateOne(connection: Connection, statement: PreparedStatement, commit: Boolean): Boolean = {
    update(connection, statement, commit) == 1
  }

  /**
    * Execute an update query (Update means anything that's not a SELECT) and test if the
    * rows affected was 1 using an active connection. This is useful for transactions
    * @param connection Active db connection
    * @param query  Your query
    * @param data Your query's data
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return True if one row was affected by this query, false otherwise
    */
  def updateOne(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Boolean = {
    withPreparedStatement(connection, query, data) {
      statement =>
        updateOne(connection, statement, commit)
    }
  }

  /**
    * Insert multiple rows and attempt to retrieve the generated AutoIds on result
    * @param sql  Your query
    * @param data Your query's data
    * @return     The generated Ids
    */
  def insertWithAutoId[R](sql: String, data: Seq[Any] = Seq.empty): Seq[Long] = {
    withConnection {
      connection =>
        withPreparedStatementGeneratingKeys(connection, sql, data) {
          statement =>
            insertWithAutoId(connection, statement, true)
        }
    }
  }

  /**
    * Insert multiple rows and attempt to retrieve the generated AutoIds on result. This version is easier for transactions
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return           The generated Ids
    */
  def insertWithAutoId(connection: Connection, statement: PreparedStatement, commit: Boolean): Seq[Long] = {
    /* ignore how many rows were inserted, you'll get the same number from auto ids list */
    statement.executeUpdate()
    val resultSet = statement.getGeneratedKeys
    if (!connection.getAutoCommit && commit) {
      connection.commit()
    }
    val autoIds = new ListBuffer[Long]
    while (resultSet.next()) {
      autoIds.append(resultSet.getLong(1))
    }
    autoIds.result()
  }

  /**
    *
    * @param connection Active db connection
    * @param query  Your query
    * @param data Your query's data
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return Number of rows affected
    */
  def insertWithAutoId(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Seq[Long] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        insertWithAutoId(connection, statement, commit)
    }
  }

  /**
    * Insert one row and attempt to retrieve the generated AutoId on result. This version is easier for transactions
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return           Some(Id) when an Id was generated, None otherwise
    */
  def insertOneWithAutoId(connection: Connection, statement: PreparedStatement, commit: Boolean): Option[Long] = {
    insertWithAutoId(connection, statement, commit).headOption
  }

  /**
    * Insert one row and attempt to retrieve the generated AutoId on result. This version is easier for transactions
    * @param connection Your active SQL connection
    * @param query      Your query
    * @param data       Your data
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return           Some(Id) when an Id was generated, None otherwise
    */
  def insertOneWithAutoId(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Option[Long] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        insertOneWithAutoId(connection, statement, commit)
    }
  }

  /**
    * Insert one row and attempt to retrieve the generated Auto Id on result
    * @param sql  Your query
    * @param data Your query's data
    * @return     Some(Id) when an Id was generated, None otherwise
    */
  def insertOneWithAutoId(sql: String, data: Seq[Any] = Seq.empty): Option[Long] = {
    insertWithAutoId(sql, data).headOption
  }


  /**
    * Insert and do not attempt to retrieve Auto Ids on result. In JDBC update() means anything that
    * is not a select so this is just an alias
    * @param sql  Your query
    * @param data Your query's data
    * @return     Number of rows affected
    */
  def insert(sql: String, data: Seq[Any] = Seq.empty): Int = {
    update(sql, data)
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return Number of rows affected
    */
  def insert(connection: Connection, statement: PreparedStatement, commit: Boolean): Int = {
    update(connection, statement, commit)
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param query      Your query
    * @param data       Your data
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return Number of rows affected
    */
  def insert(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Int = {
    withPreparedStatement(connection, query, data) {
      statement =>
        update(connection, statement, commit)
    }
  }

  /**
    * Insert and check if one row was affected. Do not attempt to retrieve Auto Ids on result
    * @param sql  Your query
    * @param data Your query's data
    * @return     True if this insert affected one row, False otherwise
    */
  def insertOne(sql: String, data: Seq[Any] = Seq.empty): Boolean = {
    insert(sql, data) == 1
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return True if one row was inserted, false otherwise
    */
  def insertOne(connection: Connection, statement: PreparedStatement, commit: Boolean): Boolean = {
    insert(connection, statement, commit) == 1
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param query      Your query
    * @param data       Your data
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return True if one row was inserted, false otherwise
    */
  def insertOne(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Boolean = {
    withPreparedStatement(connection, query, data) {
      statement =>
        insertOne(connection, statement, commit)
    }
  }

  /**
    * General query execution
    * @param sql  Your query
    * @param data Your query's data
    */
  def execute(sql: String, data: Seq[Any] = Seq.empty): Boolean = {
    withConnection {
      connection =>
        withPreparedStatement(connection, sql, data) {
          statement =>
            val success = statement.execute()
            if (!connection.getAutoCommit) {
              connection.commit()
            }
            success
        }
    }
  }
}