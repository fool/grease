package net.foolz.grease

/**
  * Helps represent a field that holds bitmask flags
  *
  * Example usage:
  *
  *   case class MerchantFlags(v: BigInt) extends Bitmask[MerchantFlag, MerchantFlags](v, MerchantFlags.apply)
  *   sealed abstract class MerchantFlag(val index: Int) extends BitmaskFlag
  *   object MerchantFlag {
  *     case object NoReceipt         extends MerchantFlag(0)
  *     case object SignatureRequired extends MerchantFlag(1)
  *   }
  *
  */
abstract class Bitmask[Flag <: BitmaskFlag, Subclass <: Bitmask[Flag, Subclass]](
  val value: BigInt,
  instantiator: BigInt => Subclass
) {
  def isSet(flag: Flag): Boolean = {
    value.testBit(flag.index)
  }

  def enable(flag: Flag): Subclass = {
    instantiator(value.setBit(flag.index))
  }

  def disable(flag: Flag): Subclass = {
    instantiator(value.clearBit(flag.index))
  }

  def set(flag: Flag, enabled: Boolean): Subclass = {
    if (enabled) {
      enable(flag)
    } else {
      disable(flag)
    }
  }
}


trait BitmaskFlag {
  /** which position this bit is. least significant bit is 1, next is 2, then 3, then 4, 5, 6... etc. */
  val index: Int
  /** the number for which this bit represents */
  def power: BigInt = {
    BigInt(2).pow(index)
  }
}