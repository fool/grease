package net.foolz.grease

import java.sql.{Connection, PreparedStatement, ResultSet, Statement}
import java.util.concurrent.Executors

import javax.sql.DataSource

import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


/**
  * Small wrapper over JDBC with a few helpers for the most common query types. This runs all
  * queries in a scala Future
  */
trait JdbcDatabaseAsync extends PreparedStatementGenerator {
  /** Any JDBC implementation will be able to give you a DataSource object */
  def dataSource: DataSource

  /** The execution context to run your queries in */
  def dbCtx: ExecutionContext

  /**
   * An execution context used to run maps/recovers in this class. All of these are local operations
   * which will be very fast. This helps keep ensure you never wait on the database to do a local map
   */
  protected val privateContext: ExecutionContext = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(10))

  /**
    * Create a Connection that is always released
    * @param function Callback to use the function in
    * @return         The result of your callback function
    */
  def withConnection[R](function: Connection => Future[R]): Future[R] = {
    try {
      val connection = dataSource.getConnection
      always(function(connection)) {
        if (connection != null && !connection.isClosed) {
          connection.close()
        }
      }
    } catch {
      case t: Throwable => Future.failed(t)
    }
  }

  /**
    * Always runs the doThis function after the Future. Exceptions in the calback function will be returned instead of
    * the underlying Future result
    */
  private def always[R](afterThis: Future[R])(doThis: => Unit): Future[R] = {
    afterThis
      .map(Success(_))(privateContext)
      .recover({ case t => Failure(t) })(privateContext)
      .map {
        result =>
          doThis
          result.get
      }(privateContext)
  }

  /**
    * Use a PreparedStatement that is always closed when your callback is done
    * @param statement The statement
    * @param function  Callback function
    * @return          The result of your callback function
    */
  def withStatement[R](statement: PreparedStatement)(function: PreparedStatement => Future[R]): Future[R] = {
    try {
      always(function(statement)) {
        if (statement != null && !statement.isClosed) {
          statement.close()
        }
      }
    } catch {
      case t: Throwable => Future.failed(t)
    }
  }

  /**
    * Prepare a query using autoPrepare() that will always close the prepared statement. This version may not
    * return auto generated keys from the ResultSet. Behavior depends on your JDBC Driver
    * @param connection Your active SQL connection
    * @param sql        Your query
    * @param data       Your query's data
    * @param function   Callback to use the prepared statement in
    * @return           Result of your callback function
    */
  def withPreparedStatement[R](connection: Connection, sql: String, data: Seq[Any])(function: PreparedStatement => Future[R]): Future[R] = {
    try {
      withStatement(connection.prepareStatement(sql)) {
        statement =>
          autoPrepare(statement, sql, data)
          function(statement)
      }
    } catch {
      case t: Throwable => Future.failed(t)
    }
  }

  /**
    * Prepare a query that should return auto generated ids (ex: Auto Increment Primary Key) using autoPrepare() and
    * always close the prepared statement.
    * @param connection Your active SQL connection
    * @param sql        Your query
    * @param data       Your query's data
    * @param function   Callback to use the prepared statement in
    * @return           Result of your callback function
    */
  def withPreparedStatementGeneratingKeys[R](connection: Connection, sql: String, data: Seq[Any])(function: PreparedStatement => Future[R]): Future[R] = {
    try {
      withStatement(connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
        statement =>
          autoPrepare(statement, sql, data)
          function(statement)
      }
    } catch {
      case t: Throwable => Future.failed(t)
    }
  }

  /**
    * SELECT Query where your callback function is not asynchronous
    * @param sql Your query
    * @param data Your query's data
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done. This must be a synchronous function
    * @param ctx The execution context used to run your callback function
    * @return The result of your callback function
    */
  def select[R](sql: String, data: Seq[Any] = Seq.empty)(callback: ResultSet => R)(implicit ctx: ExecutionContext): Future[R] = {
    withConnection {
      connection =>
        withPreparedStatement(connection, sql, data) {
          statement =>
            select(connection, statement)(callback)
        }
    }
  }

  /**
    * SELECT Query with an active connection useful for transcations
    * @param connection Active db connection
    * @param statement Prepared statement
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done. This must be a synchronous function
    * @param ctx The execution context used to run your callback function
    * @return The result of your callback function
    */
  def select[R](connection: Connection, statement: PreparedStatement)(callback: ResultSet => R)(implicit ctx: ExecutionContext): Future[R] = {
    Future {
      val resultSet = statement.executeQuery()
      if (!connection.getAutoCommit) {
        connection.commit()
      }
      resultSet
    }(dbCtx).map(resultSet => callback(resultSet))(ctx)
  }

  /**
    * SELECT Query with an active connection useful for transcations
    * @param connection Active db connection
    * @param query Your query
    * @param data Your query's data
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done. This must be a synchronous function
    * @param ctx The execution context used to run your callback function
    * @return The result of your callback function
    */
  def select[R](connection: Connection, query: String, data: Seq[Any])(callback: ResultSet => R)(implicit ctx: ExecutionContext): Future[R] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        select(connection, statement)(callback)
    }
  }

  /**
    * SELECT query where your callback function is asynchronous and returns Future
    * @param sql Your query
    * @param data Your query's data
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done
    * @param ctx The execution context used to run your callback function
    * @return The result of your callback function
    */
  def selectAsync[R](sql: String, data: Seq[Any] = Seq.empty)(callback: ResultSet => Future[R])(implicit ctx: ExecutionContext): Future[R] = {
    withConnection {
      connection =>
        withPreparedStatement(connection, sql, data) {
          statement =>
            selectAsync(connection, statement)(callback)
        }
    }
  }

  /**
    * SELECT Query with an active connection useful for transcations
    * @param connection Active db connection
    * @param statement Prepared statement
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done
    * @param ctx The execution context used to run your callback function
    * @return The result of your callback function
    */
  def selectAsync[R](connection: Connection, statement: PreparedStatement)(callback: ResultSet => Future[R])(implicit ctx: ExecutionContext): Future[R] = {
    Future {
      val resultSet = statement.executeQuery()
      if (!connection.getAutoCommit) {
        connection.commit()
      }
      resultSet
    }(dbCtx).flatMap(resultSet => callback(resultSet))(ctx)
  }

  /**
    * SELECT Query with an active connection useful for transcations
    * @param connection Active db connection
    * @param query Your query
    * @param data Your query's data
    * @param callback Your code to run with the result. Note the DB connection will not be released until your function
    *                 is done
    * @param ctx The execution context used to run your callback function
    * @return The result of your callback function
    */
  def selectAsync[R](connection: Connection, query: String, data: Seq[Any])(callback: ResultSet => Future[R])(implicit ctx: ExecutionContext): Future[R] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        selectAsync(connection, statement)(callback)
    }
  }

  /**
   * Execute an update query. Update means anything that's not a SELECT
   * @param sql  Your query
   * @param data Your query's data
   * @return Number of rows affected
   */
  def update(sql: String, data: Seq[Any] = Seq.empty): Future[Int] = {
    withConnection {
      connection =>
        withPreparedStatement(connection, sql, data) {
          statement =>
            update(connection, statement, true)
        }
    }
  }

  /**
    * Execute an update query with an active connection. Update means anything that's not a SELECT.
    * This is useful for transactions
    * @param connection Active db connection
    * @param statement  Prepared query
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return Number of rows affected
    */
  def update(connection: Connection, statement: PreparedStatement, commit: Boolean): Future[Int] = {
    Future {
      val rowsAffected = statement.executeUpdate()
      if (!connection.getAutoCommit && commit) {
        connection.commit()
      }
      rowsAffected
    }(dbCtx)
  }

  /**
    * Execute an update query with an active connection. Update means anything that's not a SELECT.
    * This is useful for transactions
    * @param connection Active db connection
    * @param query Your query
    * @param data Your query's data
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return Number of rows affected
    */
  def update(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Future[Int] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        update(connection, statement, commit)
    }
  }

  /**
   * Execute an update query (Update means anything that's not a SELECT) and test if the
   * rows affected was 1
   * @param sql  Your query
   * @param data Your query's data
   * @return True if one row was affected by this query, false otherwise
   */
  def updateOne(sql: String, data: Seq[Any] = Seq.empty): Future[Boolean] = {
    update(sql, data).map(rowsAffected => rowsAffected == 1)(privateContext)
  }

  /**
    * Execute an update query (Update means anything that's not a SELECT) and test if the
    * rows affected was 1. This version uses an active connection which is useful for transactions
    * @param connection Active db connection
    * @param statement  Prepared query
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return True if one row was affected by this query, false otherwise
    */
  def updateOne(connection: Connection, statement: PreparedStatement, commit: Boolean): Future[Boolean] = {
    update(connection, statement, commit).map(_ == 1)(privateContext)
  }

  /**
    * Execute an update query (Update means anything that's not a SELECT) and test if the
    * rows affected was 1. This version uses an active connection which is useful for transactions
    * @param connection Active db connection
    * @param query  Your query
    * @param data Your query's data
    * @param commit     Whether or not to commit the query (ends transactions)
    * @return True if one row was affected by this query, false otherwise
    */
  def updateOne(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Future[Boolean] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        updateOne(connection, statement, commit)
    }
  }

  /**
   * Insert multiple rows and attempt to retrieve the generated AutoIds on result
   * @param sql  Your query
   * @param data Your query's data
   * @return     The generated Ids
   */
  def insertWithAutoId(sql: String, data: Seq[Any] = Seq.empty): Future[Seq[Long]] = {
    withConnection {
      connection =>
        withPreparedStatementGeneratingKeys(connection, sql, data) {
          statement =>
            insertWithAutoId(connection, statement, true)
        }
    }
  }

  /**
    * Insert multiple rows and attempt to retrieve the generated AutoIds on result. This version is easier for transactions
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return           The generated Ids
    */
  def insertWithAutoId(connection: Connection, statement: PreparedStatement, commit: Boolean): Future[Seq[Long]] = {
    Future {
      /* ignore how many rows were inserted, you'll get the same number from auto ids list */
      statement.executeUpdate()
      val resultSet = statement.getGeneratedKeys
      if (!connection.getAutoCommit && commit) {
        connection.commit()
      }
      val autoIds = new ListBuffer[Long]
      while (resultSet.next()) {
        autoIds.append(resultSet.getLong(1))
      }
      autoIds.result()
    }(dbCtx)
  }

  /**
    * Insert multiple rows and attempt to retrieve the generated AutoIds on result. This version is easier for
    * transactions and prepares the query for you
    * @param connection Your active SQL connection
    * @param query      Your query
    * @param data       Query data values
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return           The generated Ids
    */
  def insertWithAutoId(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Future[Seq[Long]] = {
    withPreparedStatement(connection, query, data) {
      preparedStatement =>
        insertWithAutoId(connection, preparedStatement, commit)
    }
  }

  /**
    * Insert one row and attempt to retrieve the generated AutoId on result. This version is easier for transactions
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return           Some(Id) when an Id was generated, None otherwise
    */
  def insertOneWithAutoId(connection: Connection, statement: PreparedStatement, commit: Boolean): Future[Option[Long]] = {
    insertWithAutoId(connection, statement, commit).map(_.headOption)(privateContext)
  }

  /**
    * Insert one row and attempt to retrieve the generated AutoId on result. This version is easier for transactions
    * and prepares the statement for you
    * @param connection Your active SQL connection
    * @param query      Your query
    * @param data       Query data values
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return           Some(Id) when an Id was generated, None otherwise
    */
  def insertOneWithAutoId(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Future[Option[Long]] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        insertOneWithAutoId(connection, statement, commit)
    }
  }

  /**
    * Insert one row and attempt to retrieve the generated Auto Id on result
    * @param sql  Your query
    * @param data Your query's data
    * @return     Some(Id) when an Id was generated, None otherwise
    */
  def insertOneWithAutoId(sql: String, data: Seq[Any] = Seq.empty): Future[Option[Long]] = {
    insertWithAutoId(sql, data).map(result => result.headOption)(privateContext)
  }

  /**
   * Insert and do not attempt to retrieve Auto Ids on result. In JDBC update() means anything that
   * is not a select so this is just an alias for update(sql, data)
   * @param sql  Your query
   * @param data Your query's data
   * @return     Number of rows affected
   */
  def insert(sql: String, data: Seq[Any] = Seq.empty): Future[Int] = {
    update(sql, data)
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return Number of rows affected
    */
  def insert(connection: Connection, statement: PreparedStatement, commit: Boolean): Future[Int] = {
    update(connection, statement, commit)
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param query      Your query
    * @param data       Your data
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return Number of rows affected
    */
  def insert(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Future[Int] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        update(connection, statement, commit)
    }
  }

  /**
   * Insert and check if only one row was affected. Do not attempt to retrieve Auto Ids on result
   * @param sql  Your query
   * @param data Your query's data
   * @return     True if 1 row was affected by this query, False otherwise
   */
  def insertOne(sql: String, data: Seq[Any] = Seq.empty): Future[Boolean] = {
    insert(sql, data).map(rowsAffected => rowsAffected == 1)(privateContext)
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param statement  Your query
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return True if one row was inserted, false otherwise
    */
  def insertOne(connection: Connection, statement: PreparedStatement, commit: Boolean): Future[Boolean] = {
    update(connection, statement, commit).map(rowsAffected => rowsAffected == 1)(privateContext)
  }

  /**
    * Insert with an active connection, useful for transcations
    * @param connection Your active SQL connection
    * @param query      Your query
    * @param data       Your data
    * @param commit     True if this connection's queries should all be committed on execution, False otherwise
    * @return True if one row was inserted, false otherwise
    */
  def insertOne(connection: Connection, query: String, data: Seq[Any], commit: Boolean): Future[Boolean] = {
    withPreparedStatement(connection, query, data) {
      statement =>
        insertOne(connection, statement, commit)
    }
  }

  /**
   * General query execution
   * @param sql  Your query
   * @param data Your query's data
   */
  def execute(sql: String, data: Seq[Any] = Seq.empty): Future[Unit] = {
    withConnection {
      connection =>
        withPreparedStatement(connection, sql, data) {
          statement =>
            Future {
              statement.execute()
              if (!connection.getAutoCommit) {
                connection.commit()
              }
            }(dbCtx)
        }
    }
  }
}
