package net.foolz.grease

import java.sql.ResultSet

import scala.concurrent.{ExecutionContext, Future}

/**
  * Your data access layer can add this trait to simplify some of the queries to JdbcDatabase
  * @tparam A The model class
  */
trait QueriesAsync[A] {
  /** DB Reference */
  protected def db: JdbcDatabaseAsync
  /** A result set mapping class that maps all columns */
  protected def mapSelectStar(resultSet: ResultSet): Seq[A]

  protected def selectStar(sql: String, params: Seq[Any] = Seq.empty)(implicit ctx: ExecutionContext): Future[Seq[A]] = {
    db.select(sql, params)(mapSelectStar)
  }

  protected def selectOne(sql: String, params: Seq[Any] = Seq.empty)(implicit ctx: ExecutionContext): Future[Option[A]] = {
    selectStar(sql, params).map(_.headOption)
  }

  protected def insert(sql: String, params: Seq[Any])(implicit ctx: ExecutionContext): Future[Long] = {
    db.insertOneWithAutoId(sql, params).map(
      _.getOrElse(throw InsertFailedException(s"Attempted to insert to DB and return auto id, but no auto ids were found", sql, params)))
  }

  protected def count(sql: String, params: Seq[Any] = Seq.empty)(implicit ctx: ExecutionContext): Future[Long] = {
    db.select(sql, params)(mapExactlyOneLong)(ctx)
  }

  protected def update(sql: String, params: Seq[Any]): Future[Int] = {
    db.update(sql, params)
  }

  /** can be used after a multi-select by key to ensure all rows searched for were indeed found */
  protected def missingIds[B](ensureAllFound: Boolean, originalIds: Seq[B], receivedIds: Seq[B]): Seq[B] = {
    val receivedSet = receivedIds.toSet
    val originalIdsSet = originalIds.toSet
    originalIdsSet.diff(receivedSet).toSeq
  }

  protected def mapExactlyOneLong(resultSet: ResultSet): Long = {
    resultSet.next()
    resultSet.getLong(1)
  }
}
