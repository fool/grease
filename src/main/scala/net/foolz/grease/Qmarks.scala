package net.foolz.grease

/** Provides question marks for query strings */
object Qmarks {

  /**
    * Qmarks(0) =>  "()"
    * Qmarks(1) =>  "(?)"
    * Qmarks(2) =>  "(?,?)"
    * Qmarks(3) =>  "(?,?,?)"
    *
    *
    * @param count How many question marks
    * @return      A group of question marks
    */
  def apply(count: Int): String = {
    List.fill(count)("?").mkString("(", ",", ")")
  }

  /**
    * 0, 2 => ""
    * 1, 2 => "(?, ?)"
    * 2, 2 => "(?, ?), (?, ?)"
    * @param numGroups Group size
    * @param groupSize Size of each group
    * @return          A list of groups of question marks
    */
  def groupList(numGroups: Int, groupSize: Int): String = {
    List.fill(numGroups)(Qmarks(groupSize)).mkString(",")
  }
}
