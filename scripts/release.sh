#!/bin/sh

set -e
set -x

sbt clean release

git checkout $(git describe --abbrev=0 --tags)
v211=$(grep -oE 'crossScalaVersions := Seq.*$' build.sbt  | grep -oE '2.11.[0-9]+')
sbt "++$v211" 'clean' 'publish'
git checkout -

