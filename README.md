# Grease

Anti-ORM database framework built on JDBC.

This framework is intended for you to write your queries in SQL strings in your code base. This can be used in your
data access layer replacing ORMs like slick

Example:

```scala

object MerchantsTable {
  val Table = "`merchants`"
  
  /* column names */
  val Id = "`id`"
  val Name = "`name`"
  val RegexCol = "`regex`"
  val Flags = "`flags`"

  val Star =
    s"""$Id, $Name, $RegexCol, $Flags"""

}

class MerchantsTable(protected val db: JdbcDatabase) extends Queries[Merchant] {
  import MerchantsTable._

  private val createMerchantSql =
    s"""INSERT INTO $Table ($Name, $RegexCol) VALUES (?, ?)"""
  def create(name: String, regex: Regex): Merchant = {
    selectByName(name) match {
      case Some(tag) => tag
      case None =>
        val id = MerchantId(insert(createMerchantSql, Seq(name, regex.toString())))
        selectById(id).get
    }
  }

  private val selectStarForIdSql =
    s"""select $Star
       |from $Table
       |where $Id = ?""".stripMargin
  def selectById(id: MerchantId): Option[Merchant] = {
    selectStar(selectStarForIdSql, Seq(id)).headOption
  }

  private def selectStarForMultipleIdSql(records: Int) =
    s"""select $Star
       |from $Table
       |where $Id IN ${Qmarks(records)}""".stripMargin
  def selectMultipleById(ids: Seq[MerchantId], ensureAllFound: Boolean = true): Seq[Merchant] = {
    val idSet = ids.toSet
    val merchants = selectStar(selectStarForMultipleIdSql(idSet.size), idSet.toSeq)
    allFound(ensureAllFound, idSet, merchants.map(_.id))
    merchants
  }

  private val selectStarForNameSql =
    s"""select $Star
       |from $Table
       |where $Name = ?""".stripMargin
  def selectByName(name: String): Option[Merchant] = {
    selectOne(selectStarForNameSql, Seq(name))
  }

  private val selectStarForRegexSql =
    s"""select $Star
       |from $Table
       |where $RegexCol = ?""".stripMargin
  def selectByRegex(regex: String): Option[Merchant] = {
    selectOne(selectStarForRegexSql, Seq(regex))
  }

  private val searchSql =
    s"""
       |select $Star
       |from $Table
       |where REGEXP_LIKE(?, $Name, 'i')
     """.stripMargin
  def search(merchantName: String): Option[Merchant] = {
    selectStar(searchSql, Seq(merchantName)).headOption
  }

  private val allSql =
    s"""
       |select $Star
       |from $Table
     """.stripMargin
  def selectAll(): Seq[Merchant] = {
    selectStar(allSql)
  }

  private val renameSql = s"""UPDATE $Table SET $Name = ? WHERE $Id = ?"""
  def rename(merchantId: MerchantId, newName: String): Merchant = {
    update(renameSql, Seq(newName, merchantId))
    selectById(merchantId).get
  }

  private val changeRegexSql = s"""UPDATE $Table SET $RegexCol = ? WHERE $Id = ?"""
  def changeRegex(merchantId: MerchantId, newRegex: Regex): Merchant = {
    update(changeRegexSql, Seq(newRegex.toString(), merchantId))
    selectById(merchantId).get
  }

  private val deleteRegexSql = s"""DELETE FROM $Table WHERE $Id = ?"""
  def delete(merchantId: MerchantId): Boolean = {
    update(deleteRegexSql, Seq(merchantId)) == 1
  }

  protected def mapSelectStar(resultSet: ResultSet): Seq[Merchant] = {
    val merchants = Seq.newBuilder[Merchant]
    while (resultSet.next()) {
      merchants += Merchant(
        MerchantId(resultSet.getLong(1)),   // id
        resultSet.getString(2), // name
        new Regex(resultSet.getString(3)), // regex
        MerchantFlags(resultSet.getLong(4))   // flags
      )
    }
    merchants.result()
  }
}

```

There's more examples in the test package net.foolz.testlib.models