
test:
	sbt test

build:
	sbt publishLocal || (stty sane & exit 1)
	stty sane

# Cross build both versions, push to bintray and bitbucket
release:
	./scripts/release.sh

