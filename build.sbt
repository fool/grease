
val scala3Version = "3.1.0"

lazy val `grease` = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "net.foolz",
      scalaVersion := scala3Version,
      version      := "0.1.0", /* not used: the version is kept in version.sbt */
      crossScalaVersions := Seq(scala3Version, "2.13.7", "2.12.15", "2.11.12")
    )),
    name := "grease",
    licenses += ("MIT", url("http://opensource.org/licenses/MIT")),
    libraryDependencies += "com.h2database"         % "h2"                   % "1.4.197" % Test,
    libraryDependencies += "com.zaxxer"             % "HikariCP"             % "2.7.4"   % Test,
    libraryDependencies += "org.scalatest"          %% "scalatest"           % "3.2.10"  % Test,
    libraryDependencies += "ch.vorburger.mariaDB4j" % "mariaDB4j"            % "2.2.2"   % Test,
    libraryDependencies += "mysql"                  % "mysql-connector-java" % "5.1.46"  % Test
  )
